package lol;

public class Temperatur {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.printf( "%-13s", "Fahrenheit" );
		System.out.printf( "|" );
		System.out.printf("%10s\n", "Celsius");
		
		System.out.println("-------------------------");
		
		double a = -28.8889;
		double b = -23.3333;
		double c = -17.7778;
		double d = -6.6667;
		double e = -1.1111;
		
		System.out.printf("%-13s","-20");
		System.out.printf( "|" );
		System.out.printf( "%10.2f\n" , a);
		
		System.out.printf("%-13s","-10");
		System.out.printf( "|" );
		System.out.printf( "%10.2f\n" , b);
		
		System.out.printf("%-13s","+0");
		System.out.printf( "|" );
		System.out.printf( "%10.2f\n" , c);
		
		System.out.printf("%-13s","+20");
		System.out.printf( "|" );
		System.out.printf( "%10.2f\n" , d);
		
		System.out.printf("%-13s","+30");
		System.out.printf( "|" );
		System.out.printf( "%10.2f\n" , e);
		
	}

}
